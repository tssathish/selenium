﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Remote;

namespace YepSelen
{
    public class Program
    {
        IWebDriver c = new ChromeDriver();

        static void Main(string[] args)
        {
            //IWebDriver c = new ChromeDriver();
            //c.Navigate().GoToUrl("https://www.yepdesk.com/sign-up");

            //IWebElement element = c.FindElement(By.Name("email"));

            //element.SendKeys("sathishatAOT@gmail.com");
        }

      [SetUp]
        public void Initialise()
        {
            //c.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-up");
        }

        [Test]
        public void Browserstacktest()
        {
            IWebDriver driver;
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );
            driver.Navigate().GoToUrl("http://www.google.com/ncr");
            Console.WriteLine(driver.Title);

            IWebElement query = driver.FindElement(By.Name("q"));
            query.SendKeys("Salvador Dali");
            query.Submit();
            Console.WriteLine(driver.Title);

            driver.Quit();
        }

        [Test]
        public void ExecuteSignup()
        {
            //IWebElement element = c.FindElement(By.Name("email"));

            //element.SendKeys("sathishatAOT@gmail.com");

            IWebDriver driver;
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );


            driver.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-up");
            YepSetMethods.EnterText(driver, "email", "sathishatAOT2@gmail.com", "Name");
            YepSetMethods.EnterText(driver, "firstName", "Sathish", "Name");
            YepSetMethods.EnterText(driver, "lastName", "TS", "Name");
            YepSetMethods.EnterText(driver, "password", "pa@ssw0rd", "Name");

            YepSetMethods.Click(driver, "btn-signup", "Id");

            Thread.Sleep(10000);
            //c.Close();
            //c.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-in-options");
            driver.FindElement(By.XPath("//span[contains (@class,'avatar avatar-online')]")).Click();
            driver.FindElement(By.XPath("//a[contains (@class,'dropdown-menu-footer-btn')]")).Click();

            driver.Quit();
            //c.FindElement(By.ClassName("dropdown-menu-footer-btn")).Click();
            //c.FindElement(By.XPath("//span[contains (@class,'dropdown-menu-footer-btn')][contains(text(),'Logout')]")).Click();
        }

        [Test]
        public void ExecuteSignIn()
        {
            c.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-in");
            YepSetMethods.EnterText(c, "login-username", "sathishatAOT2@gmail.com", "Id");
            YepSetMethods.EnterText(c, "login-password", "pa@ssw0rd", "Id");
            //var element = c.FindElement(By.XPath("//a[contains(text(), 'Login')]"));
            //element.Click();
            c.FindElement(By.XPath("//button[text()='Login']")).Click();
        }

        [TearDown]
        public void CleanUp()
        {

        }
    }
}
