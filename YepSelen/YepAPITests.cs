﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace ServiceApiTest
{
    [TestFixture]
    public class YepApiTests
    {
        private IWebDriver _webDriver;
        string BaseUrl = "https://www.625.yepdesk.com";
        [SetUp]
        public void Setup()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Window.Maximize();
        }
        [Test]
        public void TestEvents()
        {
            YepApiResponse apiResponse = new YepApiResponse();
            string expectedResult = apiResponse.GetResponse("/rest/v1/events");
            //Thread.Sleep(5000);
            Console.WriteLine("-------------------- Web Page Displayed ------------------");
            _webDriver.Navigate().GoToUrl(BaseUrl + "/rest/v1/events");
            IWebElement responseElement = _webDriver.FindElement(By.TagName("pre"));
            string displayedResponse = responseElement.Text;
            Console.WriteLine(displayedResponse);
            Assert.IsTrue(expectedResult.Equals(displayedResponse));
        }
        [Test]
        public void TestEventCategories()
        {
            const string categories = "/rest/v1/India/Chennai/events/categories";
            YepApiResponse apiResponse = new YepApiResponse();
            string expectedResult = apiResponse.GetResponse(categories);
            Thread.Sleep(5000);
            Console.WriteLine("-------------------- Web Page Displayed ------------------");
            _webDriver.Navigate().GoToUrl(BaseUrl + categories);
            IWebElement responseElement = _webDriver.FindElement(By.TagName("pre"));
            string displayedResponse = responseElement.Text;
            Console.WriteLine(displayedResponse);
            Assert.IsTrue(expectedResult.Equals(displayedResponse));
        }

        [TearDown]
        public void Teardown()
        {
            _webDriver.Close();
            _webDriver.Quit();
        }
    }
}