﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using YepSelen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Threading;
using Protractor;
using OpenQA.Selenium.Chrome;

namespace YepSelen.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void BrowserstacktestTest()
        {
            IWebDriver driver;
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );
            driver.Navigate().GoToUrl("http://www.google.com/ncr");
            Console.WriteLine(driver.Title);

            IWebElement query = driver.FindElement(By.Name("q"));
            query.SendKeys("AOT Technology");
            query.Submit();
            Console.WriteLine(driver.Title);

            driver.Quit();
        }

        [TestMethod()]
        public void ExecuteSignup()
        {
            //IWebElement element = c.FindElement(By.Name("email"));

            //element.SendKeys("sathishatAOT@gmail.com");

            IWebDriver driver;
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );


            driver.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-up");
            YepSetMethods.EnterText(driver, "email", "sathishatAOT2@gmail.com", "Name");
            YepSetMethods.EnterText(driver, "firstName", "Sathish", "Name");
            YepSetMethods.EnterText(driver, "lastName", "TS", "Name");
            YepSetMethods.EnterText(driver, "password", "pa@ssw0rd", "Name");

            YepSetMethods.Click(driver, "btn-signup", "Id");

            Thread.Sleep(10000);
            //c.Close();
            //c.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-in-options");
            driver.FindElement(By.XPath("//span[contains (@class,'avatar avatar-online')]")).Click();
            driver.FindElement(By.XPath("//a[contains (@class,'dropdown-menu-footer-btn')]")).Click();

            driver.Quit();
            //c.FindElement(By.ClassName("dropdown-menu-footer-btn")).Click();
            //c.FindElement(By.XPath("//span[contains (@class,'dropdown-menu-footer-btn')][contains(text(),'Logout')]")).Click();
        }


        [TestMethod]
         public void ExecuteSignIn()
        {
            IWebDriver driver;
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );

            driver.Navigate().GoToUrl("https://www.625.yepdesk.com/sign-in");
            YepSetMethods.EnterText(driver, "login-username", "sathishatAOT2@gmail.com", "Id");
            YepSetMethods.EnterText(driver, "login-password", "pa@ssw0rd", "Id");
            //var element = c.FindElement(By.XPath("//a[contains(text(), 'Login')]"));
            //element.Click();
            driver.FindElement(By.XPath("//button[text()='Login']")).Click();//
            Thread.Sleep(50000);
            driver.Quit();
        }

        //[TestMethod]
        //public void ProtractorTestExecuteSignIn()
        //{
        //    using (var ngDriver = new NgWebDriver(new ChromeDriver()))
        //    {
        //        ngDriver.Url = "https://www.625.yepdesk.com/sign-in";
        //        ngDriver.FindElement(NgBy.Model("user.email")).SendKeys("sathishatAOT2@gmail.com");
        //        ngDriver.FindElement(NgBy.Model("user.password")).SendKeys("pa@ssw0rd");
        //        //ngDriver.FindElement(NgBy.  By.CssSelector("[ng-click='loginUser(user)()']"));
        //        //ngDriver.FindElement(By.css)
        //        //Assert.AreEqual("Hello Julie!", ngDriver.FindElement(NgBy.Binding("yourName")).Text);

        //    }
        //}

        [TestMethod()]
        public void Testfordemo()
        {
            IWebDriver driver;
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );
            driver.Navigate().GoToUrl("http://www.google.com/ncr");
            Console.WriteLine(driver.Title);

            IWebElement query = driver.FindElement(By.Name("q"));
            query.SendKeys("Salesforce");
            query.Submit();
            Console.WriteLine(driver.Title);

            driver.Quit();
        }

    }
}