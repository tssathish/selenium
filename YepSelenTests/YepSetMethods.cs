﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YepSelen
{
    class YepSetMethods
    {
        public static void EnterText(IWebDriver driver, string element, string value, string elementtype)
        {
            if (elementtype == "Name")
                driver.FindElement(By.Name(element)).SendKeys(value);
            if (elementtype == "Id")
                driver.FindElement(By.Id(element)).SendKeys(value);
        }

        public static void Click(IWebDriver driver, string element, string elementtype)
        {
            if (elementtype == "Id")
                driver.FindElement(By.Id(element)).Click();
        }
        //
    }
}
